/*

cosmo_sim_1d -- A simple 1D n-body time integrator with Zel'dovich ICs
Copyright (c) 2019 Oliver Hahn

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software. Scientific publications
that make use of the Software or substantial portions of the Software shall
acknowledge this and refer a link to the Software's repository.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#pragma omp

#include <cmath>

namespace ics{
//... initial conditions class (to be passed as template parameter to solver)
//... just adjust getdisp function for other ICs
struct plane_wave
{
    double a, amp, vfac;
    double eps, alpha;

    // Hubble constant
    const double H0 = 1.0;

    /*! constructor for plane wave initial conditions with cterm * sin^4 perturbation */
    explicit plane_wave(double astart, double cterm) : a(astart), eps(cterm)
    {
        // shell-crossing at a=across desired, fixed to a=1 here.
        const double across = 1.0; 

        // this is set so that sin^6 term cancels sin^4 term in integral over box:
        alpha = -6.0*eps/5.0;

        // amplitude for sin(q) wave to shell-cross at a=across:
        amp = 1.0 / (2.0 * M_PI * across); 

        // conversion factor between displacement and velocity:
        vfac = H0*std::sqrt(a); 
    }

    //! change the time 
    void set_aexp( double aexp ){
        a = aexp;
        vfac = H0*std::sqrt(a);
    }

    //! return initial displacement at astart, q in -0.5...0.5
    double getdisp(double q) const 
    { 
        return amp * a * (-std::sin(2.0 * M_PI * q) + eps*std::pow(std::sin(2.0 * M_PI * q),4.0) + alpha * std::pow(std::sin(2.0*M_PI*q),6.0) );
    }

    //! return initial velocity at astart, q in -0.5...0.5
    double getvel(double q) const
    {
        return getdisp(q) * vfac;
    }
};

}; // end namespace ics
