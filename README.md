# COSMO_SIM_1D 

A simple 1D n-body time integrator with Zel'dovich ICs, used for the simulations in Rampf et al. 2019 [(arXiv:1912.00868)](https://arxiv.org/abs/1912.00868).

## Features
* exact force calculation by particle sorting
* uses a DKD symplectic integrator (2nd order accurate)
* written in modernish C++ (requires C++14 compiler)

## License
You can use the code according to the included license (see each source
file or the LICENSE file) and modify the code freely in accordance with it. 
If you use it (or significant portions of it) in a scientific publication, 
then a link to this repository and a citation to [Rampf et al. 2019](https://arxiv.org/abs/1912.00868) should
be included.

## Build and Running Instructions
Compilation requires a C++14 compatible C++ compiler (e.g. g++)

    g++ -O3 -std=c++14 cosmo_sim_1d.cc -o cosmo_sim_1d

Then it can be called with the following command line options

    ./cosmo_sim_1d -a 0.9 -A 1.1 -s 8192 -n 8192

Evolution is computed in time=expansion factor a.

The command line options are:

* -a to specify the starting time for the symplectic integration (end time for zeldovich step)
* -A to specify the end time for the symplectic integration
* -s to specify the number of time steps (equally spaced in log a)
* -n to specify the number of particles used
* -c value of the symmetry-breaking sin^4 term (default=0)

## Output results
After finishing a run, we write out 3 files, one with the solution, one with the ICs, one with Zel'dovich solution at end time.
