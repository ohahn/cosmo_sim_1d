/*

cosmo_sim_1d -- A simple 1D n-body time integrator with Zel'dovich ICs
Copyright (c) 2019 Oliver Hahn

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software. Scientific publications
that make use of the Software or substantial portions of the Software shall
acknowledge this and refer a link to the Software's repository.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#pragma once

#include <array>
#include <vector>
#include <cmath>

#include <iostream>
#include <iomanip>

namespace nbody{

constexpr std::size_t nfields{5};

//... basic particle structure, just holds all variables, and provides access-functions
struct particle : public std::array<double, nfields>
{
    particle(double q_, double x_, double v_)
        : std::array<double, nfields>({{q_, x_, v_}}) {}
    
    particle(double q_, double x_, double v_, double a_ )
        : std::array<double, nfields>({{q_, x_, v_, a_}}) {}
    
    particle(double q_, double x_, double v_, double a_, double aZA_ )
        : std::array<double, nfields>({{q_, x_, v_, a_, aZA_}}) {}
    
    //.. Lagrangian coordinate
    double &q() { return (*this)[0]; }
    double q() const { return (*this)[0]; }

    //.. Displacement from Lagrangian coordinate
    double &Psi() { return (*this)[1]; }
    double Psi() const { return (*this)[1]; }

    //.. velocity (from conjugate momentum)
    double &v() { return (*this)[2]; };
    double v() const { return (*this)[2]; }

    //.. acceleration
    double &a() { return (*this)[3]; };
    double a() const { return (*this)[3]; }

    //.. acceleration according to Zel'dovich approx.
    double &aZA() { return (*this)[4]; };
    double aZA() const { return (*this)[4]; }

    // compute the actual position [0,1) from displacement
    auto pos() const { return std::fmod(1.0 + q() + Psi(), 1.0); }

    // sort function on actual position
    bool operator<(const particle &o) const { return this->pos() < o.pos(); }
};

//! N-body system class, providing integrators
template <typename ic_creator>
struct system
{
    //! current expansion factor of the system
    double aexp_;

    //! number of steps carried out
    std::size_t step_;

    //! vector holding particle data
    std::vector<particle> particles_;

    //! constructor for N-body system with nump_ particles, starting time astart and end time aend
    system( const ic_creator& icgen, std::size_t nump_, double astart, double aend ) : aexp_(astart), step_(0)
    {
        // don't forget that p = v / sqrt(a) in our units
        particles_.reserve(nump_);
        for (std::size_t i = 0; i < nump_; ++i)
        {
            double q = (double(i) + 0.5) / nump_ - 0.5;
            double afac = std::sqrt(astart) * astart/aend;
            particles_.emplace_back(particle{q, icgen.getdisp(q), icgen.getvel(q), 0.0, icgen.getvel(q)/afac});
        }
    }

    //! begin-iterator for range-based looping
    auto begin() const { return particles_.begin(); }

    //! end-iterator for range-based looping
    auto end() const { return particles_.end(); }

    //! return current cosmic expansion factor
    auto aexp() const { return aexp_; }

    //! compute particle acceleration 
    void compute_acc(void)
    {
        std::sort(particles_.begin(), particles_.end());

        double mean_pos{0.0};
        for (auto &p : particles_)
        {
            mean_pos += p.pos();
        }
        mean_pos = mean_pos / particles_.size() - 0.5;

        for (std::size_t i = 0; i < particles_.size(); ++i)
        {
            auto acc =
                -((double(i) + 0.5) / (particles_.size()) - (particles_[i].pos() - mean_pos));
            particles_[i].a() = acc;
        }
    }

    //! time integration function, integrate the system from aini to aend in nsteps (equally spaced in log a)
    void step(double aini, double aend, unsigned nsteps )
    {
        const double H0 = 1.0;
        const double logaini = std::log(aini), logaend = std::log(aend), dloga = (logaend-logaini)/nsteps;

        aexp_ = aini;

        // main stepper loop
        for( unsigned istep = 0; istep < nsteps; ++istep ){
            
            double a0 = std::exp( logaini + istep*dloga );
            double a1 = std::exp( logaini + (istep+1)*dloga );
            
            const double eps_alpha = -2.0 * (std::pow(a1, -0.5) - std::pow(a0, -0.5));
            const double eps_beta = 2.0 * (std::pow(a1, 0.5) - std::pow(a0, 0.5));
            
            // drift
            for (auto &p : particles_)
            {
                p.Psi() += 0.5 * p.v() * eps_alpha / H0;
            }

            // kick
            this->compute_acc();

            const double accfac = 1.5 * H0 * H0;
            for (std::size_t i = 0; i < particles_.size(); ++i)
            {
                particles_[i].v() += particles_[i].a() * accfac / H0 * eps_beta;
            }

            // drift
            for (auto &p : particles_)
            {
                p.Psi() += 0.5 * p.v() * eps_alpha / H0;
            }

            aexp_ = a1;            
        }
    }
};

}; // end namespace nbody

//! output n-body system to an ostream...
template <typename ic_creator>
std::ostream &operator<<(std::ostream &stream, nbody::system<ic_creator> &nb)
{
    std::sort(nb.particles_.begin(), nb.particles_.end(),
              [](auto p1, auto p2) { return p1.q() < p2.q(); });

    stream << "# snapshot at a = " << std::setprecision(10) << nb.aexp() << std::endl;
    stream << "# "
           << std::setw(14) << "q"
           << std::setw(16) << "psi"
           << std::setw(16) << "v"
           << std::setw(16) << "acc"
           << std::setw(16) << "acc_Zeldo"
           << std::setw(16) << "x"
           << std::endl;

    for (auto &p : nb.particles_)
    {
        for (auto x : p)
        {
            stream << std::setw(20) << std::setprecision(14) << x << " ";
        }
        stream << std::setw(20) << std::setprecision(14) << ((p.pos() > 0.5)? p.pos()-1.0 : p.pos());
        stream << std::endl;
    }
    return stream;
}