/*

cosmo_sim_1d -- A simple 1D n-body time integrator with Zel'dovich ICs
Copyright (c) 2019 Oliver Hahn

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software. Scientific publications
that make use of the Software or substantial portions of the Software shall
acknowledge this and refer a link to the Software's repository.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/


#include <array>
#include <vector>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <cassert>

#include "cxxopts.hh"

#include "nbody.hh"
#include "ics.hh"

//... main routine, parse command line options, carry out integration, write output
int main( int argc, char **argv )
{
    //... parse command line options
    cxxopts::Options options("cosmo_sim_1d", "Cosmological N-body for 1D cosmological simulations");
    options.add_options()
        ("help",     "Print help")
        ("a,astart", "Starting a (default: 0.1)", cxxopts::value<double>())
        ("A,aend",   "Final a (default: 1.0)", cxxopts::value<double>())
        ("s,steps",  "steps to solution (default: 1024)", cxxopts::value<size_t>())
        ("c,cterm",  "value of symmetry-breaking c term (default: 0.0)", cxxopts::value<double>())
        ("n,nparticles","number of particles (default: 8192)", cxxopts::value<std::size_t>())
        ;

    if( argc == 1 ){
        std::cout << "No command line options given, running with defaults -- and printing help:\n\n";
        std::cout << options.help() << std::endl << std::endl;
    }else{
        std::cout << "Cosmological N-body for 1D cosmological simulations" << std::endl;
    }

    auto result = options.parse(argc, argv);

    std::size_t numpart  = 8192;
    std::size_t numsteps = 1024;
    double astart = 0.1, aend = 1.0, cterm = 0.0;

    if (result.count("help"))
    {
      std::cout << options.help() << std::endl;
      exit(0);
    }
    if( result.count("a") ){
        astart = result["a"].as<double>();
    }
    if( result.count("A") ){
        aend = result["A"].as<double>();
    }
    if( result.count("s") ){
        numsteps = result["s"].as<size_t>();
    }
    if( result.count("n") ){
        numpart = result["n"].as<std::size_t>();
    }
    if( result.count("c") ){
        cterm = result["c"].as<double>();
    }

    //... output runtime parameters
    std::cout << "Running with:\n   numpart = " << numpart << "\n   numsteps = " << numsteps << "\n   astart = " << astart 
            << "\n   aend = " << aend <<  "\n   cterm = " << cterm <<  std::endl << std::endl;
    
    //... initial conditions generator with c_term * sin^4 perturbation for a=astart
    ics::plane_wave ic_generator( astart, cterm );

    //... N-body system, with ICs at astart
    nbody::system<ics::plane_wave> nb(ic_generator, numpart, astart, aend);

    //... dummy N-body system where we generate ICs at aend (only used for comparison with Zel'dovich)
    ic_generator.set_aexp(aend);
    nbody::system<ics::plane_wave> nb_ana(ic_generator, numpart, aend, aend);
    
    //... compute initial acceleration
    nb.compute_acc();

    //... output initial condition data at astart
    {
        std::ofstream ofs("output_initial.txt");
        ofs << nb << std::endl;
    }

    //... perform time integration from astart to aend using numsteps (equally spaced in log a)
    std::cout << "Evolving..." << std::flush;
    nb.step(astart, aend, numsteps );
    
    //... output result of time integration from astart to aend
    {
        std::stringstream ss;
        ss << "output_nsteps_" << numsteps << "_astart_" << astart << "_aend_" << aend << "_c" << cterm <<  ".txt";
        std::ofstream ofs(ss.str().c_str());
        ofs << nb << std::endl;
    }

    //... output Zeldovich solution at aend
    {
        std::ofstream ofs("output_ana_end.txt");
        ofs << nb_ana << std::endl;
    }

    std::cout << "\nDone.\n" << std::endl;

    return 1;
}
